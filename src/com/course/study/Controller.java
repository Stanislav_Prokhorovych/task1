package com.course.study;

import java.util.Scanner;

/**
 * @author Stanislav Prokhorovych
 */

public class Controller {
    private Notebook model;
    private View view;

    public Controller(Notebook model, View view) {
        this.model = model;
        this.view = view;
    }

    public void processUser() {
        Scanner scanner = new Scanner(System.in);

        String surname = inputValueWithScanner(scanner, View.REGEX_SURNAME, View.PLEASE_ENTER_SURNAME);
        String name = inputValueWithScanner(scanner, View.REGEX_NAME, View.PLEASE_ENTER_NAME);
        String middleName = inputValueWithScanner(scanner, View.REGEX_MIDDLE_NAME, View.PLEASE_ENTER_MIDDLE_NAME);
        printInitials(surname, name);
        String nickname = inputValueWithScanner(scanner, View.REGEX_NICKNAME, View.PLEASE_ENTER_NICKNAME);
        String comment = inputValueWithScanner(scanner, View.REGEX_COMMENT, View.PLEASE_ENTER_COMMENT);
        String phone = inputValueWithScanner(scanner, View.REGEX_PHONE, View.PLEASE_ENTER_PHONE);
        String email = inputValueWithScanner(scanner, View.REGEX_MAIL, View.PLEASE_ENTER_MAIL);
        String skype = inputValueWithScanner(scanner, View.REGEX_SKYPE, View.PLEASE_ENTER_SKYPE);

        view.printMessage(View.SUCCESS);

        model.setSurname(surname);
        model.setName(name);
        model.setMiddleName(middleName);
        model.setNickname(nickname);
        model.setComment(comment);
        model.setGroup(Group.ADMIN);
        model.setPhone(phone);
        model.setEmail(email);
        model.setSkype(skype);

        System.out.println(model.toString());
    }

    /**
     *
     * @param surname surname to be printed
     * @param name name to be printed
     */
    private void printInitials(String surname, String name) {
        view.printMessage(surname + View.SPACE + name.charAt(0) + View.POINT);
    }

    /**
     *
     * @param scanner scanner for reading input
     * @param regex for checking if input match
     * @param hintMessage message about what information to enter
     * @return string that match regex
     */
    private String inputValueWithScanner(Scanner scanner, String regex, String hintMessage) {
        String string ="";
        boolean inputIsRight = false;
        while(!inputIsRight) {
            view.printMessage(hintMessage);
            string = scanner.nextLine();
            if(string.matches(regex)) {
                inputIsRight = true;
            } else {
                view.printMessage(View.WRONG_INPUT);
            }
        }
        return string;
    }
}
