package com.course.study;

/**
 * @author Stanislav Prokhorovych
 */

public enum Group {
    USER,
    ADMIN,
}
