package com.course.study;

/**
 * @author Stanislav Prokhorovych
 */

public class View {
    public static final String REGEX_SURNAME = "^([A-Z]+)([a-z]*)$";
    public static final String REGEX_NAME = "^([A-Z]+)([a-z]*)$";
    public static final String REGEX_MIDDLE_NAME = "^([A-Z]+)([a-z]*)$";
    public static final String REGEX_NICKNAME = "^([A-Z]+)([a-z_-]*)$";
    public static final String REGEX_COMMENT = "\\w+";
    public static final String REGEX_MAIL = "^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$";
    public static final String REGEX_PHONE = "^\\+\\d{2}\\(\\d{3}\\)\\d{3}-\\d{2}-\\d{2}$";
    public static final String REGEX_SKYPE = "live:\\w+";

    public static final String PLEASE_ENTER_SURNAME = "Please enter a surname";
    public static final String PLEASE_ENTER_NAME = "Please enter a name";
    public static final String PLEASE_ENTER_MIDDLE_NAME = "Please enter a middle name";
    public static final String PLEASE_ENTER_NICKNAME = "Please enter a nickname";
    public static final String PLEASE_ENTER_COMMENT = "Please enter a comment";
    public static final String PLEASE_ENTER_PHONE = "Please enter a phone";
    public static final String PLEASE_ENTER_MAIL = "Please enter an email";
    public static final String PLEASE_ENTER_SKYPE = "Please enter a skype";

    public static final String WRONG_INPUT = "Wrong input try again";

    public static final String SPACE = " ";
    public static final String POINT = ".";

    public static final String SUCCESS = "Thank you, data has been processed";

    /**
     * @param message message to be printed
     */
    public void printMessage(String message) {
        System.out.println(message);
    }
}
